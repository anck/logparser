from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QFont

class FilePathLineEdit:
    """
    Class includes defines the File Text Area widget.
    """

    def __init__(self):
        """
        constructor.
        """
        self.file_text_area = QLabel()
        self.myFont = QFont()
        self.myFont.setBold(True)
        self.file_text_area.setFont(self.myFont)

    def get_file_line_edit(self):
        """
        return QtextEdit.
        :return: QtextEdit
        """
        return self.file_text_area


    def set_file_line_edit(self , blurb):
        """
        Sets the text area with given blurb.
        :param blurb: the text to be displayed.
        :return: None.
        """
        self.file_text_area.setText(str(blurb))
