from PyQt5.QtWidgets import QApplication, QTreeView, QTreeWidget, QTextEdit, QTreeWidgetItem
from collections import OrderedDict


class MainTreeWidget:
    """
    Tree widget class which displays all the keys of the dictionary.
    """

    def __init__(self):
        """
        Constructor.
        """
        self.text = QTextEdit()
        self.widget = QTreeWidget()
        self.widget.setHeaderLabel("Keys")

    def get_tree_widget(self):
        """
        return QTreeWidget
        :return: QTreeWidget.
        """
        return self.widget


    def fill_widget(self, widget, dic):
        """
        function which checks calls the fill_item function to populate the f=tree widget.
        :param widget: is the root node of the tree widget.
        :param dic: is the dictinoary or list use to populate the tree widget.
        :return:
        """
        widget.clear()
        self.fill_item(widget.invisibleRootItem(), dic)


    def fill_item(self, item, value):
        """
        Is a recursive function which checks if the input is a dictionary or a list.
        If the input does not matches dic or list, it displays the str value.
        :param item: is the root node of the tree widget.
        :param value: is the dictinoary or list use to populate the tree widget.
        :return:
        """
        print("LOG: filling Tree recursive call.")
        item.setExpanded(True)
        if type(value) is OrderedDict:
            for key, val in value.items():
                child = QTreeWidgetItem()
                child.setText(0, str(key))
                item.addChild(child)
        elif type(value) is list:
            for val in value:
                child = QTreeWidgetItem()
                item.addChild(child)
                if type(val) is dict:
                    child.setText(0, '[dict]')
                elif type(val) is list:
                    child.setText(0, '[list]')
                else:
                    child.setText(0, str(val))
                child.setExpanded(True)
        else:
            child = QTreeWidgetItem()
            child.setText(0, str(value))
            item.addChild(child)

    def set_header_label(self, blurb):
        """
        Sets header on tree widget.
        :param blurb: text to set the header
        :return:
        """
        self.widget.setHeaderLabel(blurb)