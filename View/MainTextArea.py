from PyQt5.QtWidgets import QTextEdit


class MainTextArea:
    """
    Class includes defines the Text Area widget.
    """

    def __init__(self):
        """
        constructor.
        """
        self.text_area = QTextEdit()
        self.text_area.setReadOnly(True)

    def get_text_area(self):
        """
        return QtextEdit.
        :return: QtextEdit
        """
        return self.text_area


    def set_text_area(self , blurb):
        """
        Sets the text area with given blurb.
        :param blurb: the text to be displayed.
        :return: None.
        """
        self.text_area.setText(str(blurb))
