from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QWidget
from PyQt5.QtWidgets import QTextEdit, QPushButton, QVBoxLayout, QHBoxLayout, QAction, qApp, QLabel
from View.MainTreeWidget import MainTreeWidget
from View.MainTextArea import MainTextArea
from View.FilePathTextEdit import FilePathLineEdit
from collections import OrderedDict


class WorkView:
    """
    This is the main widget class containing the tree widget and text area.
    Uses Horizontal and Vertical layout to arrange the child widgets.
    """
    def __init__(self):
        """
        constructor.
        """
        super().__init__()
        self.main_tree_widget = MainTreeWidget()
        self.main_text_area = MainTextArea()
        self.file_path = FilePathLineEdit()
        self.label = QLabel()

    def get_tree_widget(self):
        """
        Returns the tree widget.
        :return:
        """
        return self.main_tree_widget.get_tree_widget()

    def get_text_area(self):
        """
        Return the text area.
        :return:
        """
        return self.main_text_area.get_text_area()

    def set_text_area(self, blurb):
        """
        sets the area with blurb
        :param blurb:
        :return:
        """
        self.main_text_area.set_text_area(blurb)

    def fill_tree_widget(self, dic):
        """
        fill the tree widget with the dictionary
        :return:
        """
        self.main_tree_widget.fill_widget(self.main_tree_widget.get_tree_widget(), dic)

    def get_work_area(self):
        """
        Returns a horizontal lay out with TreeWidget and Text area.
        :return:
        """
        v_layout =QVBoxLayout()
        v_layout.addWidget(self.label)
        v_layout.addWidget(self.file_path.get_file_line_edit(), 1)
        h_layout = QHBoxLayout()
        h_layout.addWidget(self.main_tree_widget.get_tree_widget())
        h_layout.addWidget(self.main_text_area.get_text_area())
        v_layout.addLayout(h_layout)
        return v_layout

    def get_file_path(self):
        """
        Return the text area.
        :return:
        """
        return self.file_path

    def set_file_path(self, blurb):
        """
        sets the area with blurb
        :param blurb:
        :return:
        """
        self.file_path.set_file_line_edit(blurb)

    def set_label_text(self, blurb):
        """
        sets the area with blurb
        :param blurb:
        :return:
        """
        self.label.setText(blurb)