import sys, os
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QWidget
from PyQt5.QtWidgets import QTextEdit, QPushButton, QVBoxLayout, QHBoxLayout, QAction, qApp, QLabel
from PyQt5.QtCore import Qt
from View.WorkView import WorkView

from collections import OrderedDict


class MainWidget(QWidget):
    """
    This is the main widget class containing the tree widget and text area.
    Uses Horizontal and Vertical layout to arrange the child widgets.
    """
    def __init__(self):
        """
        constructor.
        """
        super().__init__()
        self.server_work_view = WorkView()
        self.client_work_view = WorkView()
        self.cdf_server_dic = OrderedDict()
        self.cdf_client_dic = OrderedDict()
        self.client_work_view.get_tree_widget().itemSelectionChanged.connect(self.on_click_tree_item)
        self.server_work_view.get_tree_widget().itemSelectionChanged.connect(self.on_click_tree_item)
        self.server_open_button = QPushButton("open server csv", self)
        self.client_open_button = QPushButton("open client csv", self)


    def init_ui(self):
        """
        initializes the Main widget.
        :return:
        """
        v_layout = QVBoxLayout()
        logo_label = QLabel()
        logo_label.setPixmap(QPixmap("../Img/Citrix_Logo_Black.jpg"))
        logo_label.setAlignment(Qt.AlignCenter)
        v_layout.addWidget(logo_label)
        self.initialize_server_buttons()
        self.initialize_client_buttons()
        v_layout.addLayout(self.server_work_view.get_work_area())
        v_layout.addWidget(self.server_open_button)
        v_layout.addLayout(self.client_work_view.get_work_area())
        v_layout.addWidget(self.client_open_button)
        self.setLayout(v_layout)

    def initialize_server_buttons(self):
        """

        :return:
        """
        self.server_open_button.setToolTip('click to open a server cdf log in csv file.')
        self.server_open_button.setFixedSize(150, 50)


    def initialize_client_buttons(self):
        """

        :return:
        """
        self.client_open_button.setToolTip('click to open a client cdf log in csv file.')
        self.client_open_button.setFixedSize(150, 50)

    def get_server_button(self):
        """

        :return:
        """
        return self.server_open_button

    def get_client_button(self):
        """

        :return:
        """
        return self.client_open_button

    def fill_server_work_area(self, server_dic, file_name):
        """
        Used to fill the Tree Widget. Should be called from the Main View class.
        :param server_dic: Keys will populate the Tree widget.
        :return:
        """
        self.cdf_server_dic = server_dic
        self.server_work_view.fill_tree_widget(server_dic)
        self.server_work_view.set_file_path(str(file_name))
        self.server_work_view.set_label_text("Server:")

    def fill_client_work_area(self, client_dic, file_name):
        """
        Used to fill the Tree Widget. Should be called from the Main View class.
        :param client_dic: Keys will populate the Tree widget.
        :return:
        """
        self.cdf_client_dic = client_dic
        self.client_work_view.fill_tree_widget(client_dic)
        self.client_work_view.set_file_path(str(file_name))
        self.client_work_view.set_label_text("Client:")

    def on_click_tree_item(self):
        """
        Manages the on click event for TreeWidget. Sets the text area with value for the current key.
        :return: None.
        """
        server_selection = self.server_work_view.get_tree_widget().selectedItems()
        client_selection = self.client_work_view.get_tree_widget().selectedItems()

        if server_selection:
            self.server_work_view.set_text_area(self.cdf_server_dic[server_selection[0].text(0)])

        if client_selection:
            self.client_work_view.set_text_area(self.cdf_client_dic[client_selection[0].text(0)])


class MainWindow(QMainWindow):
    """
    Main window contains the main widget class. May be moved to a different class\module.
    Controller would need to create a object of this class to initiate the application.
    Acts as a View object giving access to all the UI elements.
    """
    def __init__(self):
        """
        constructor.
        """
        super(MainWindow, self).__init__()
        self.main_widget = MainWidget()
        self.setCentralWidget(self.main_widget)
        self.main_widget.init_ui()
        self.setWindowTitle("Seamless Log Parser")
        self.file_dialog = QFileDialog()
        self.show()

    def set_window_icon(self,icon_path):
        if icon_path:
            print(os.path.isfile(icon_path))
            self.setWindowIcon(QIcon(QPixmap(icon_path)))

    def fill_server_tree(self, server_dic, file_name):
        """
        function to fill tree widget.
        :param dic: Keys would populate the tree widget. is able to handle Lists as well.
        :return: None.
        """
        if server_dic:
            self.main_widget.fill_server_work_area(server_dic, str(file_name))

    def fill_client_tree(self, client_dic, file_name):
        """
        function to fill tree widget.
        :param dic: Keys would populate the tree widget. is able to handle Lists as well.
        :return: None.
        """
        if client_dic:
            self.main_widget.fill_client_work_area(client_dic, file_name)

    def get_file_dialouge(self):
        return self.file_dialog

    def get_server_btn(self):
        """

        :return:
        """
        return self.main_widget.get_server_button()

    def get_client_btn(self):
        """

        :return:
        """
        return self.main_widget.get_client_button()