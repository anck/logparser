import csv
import os
from collections import OrderedDict


class CSVReader:
    """
    Utility class to read csv files.
    """

    def readCSVFile(self, filepath="../DemoFiles/exampleCSV.csv", keyRow=0):
        """
        reads csv file given at filepath, and uses index keyRow
        to generate keys in the dictionary.
        TODO: add error handling to file IO.
        :param filepath: specify the file to open. If not a file return string.
        :param keyRow: specify row to set dic keys. They should be unique. Default to '0' column.
        :return: dic if file can be opened. If not a file it will return a string with error message.
        """

        #Orderddictinary
        outputDic = OrderedDict()
        if os.path.isfile(filepath):
            print("File found")
            #add try catch
            with open(filepath, mode='r') as fin:
                reader = csv.reader(fin)
                for row in reader:
                    key = row[keyRow]
                    value = row
                    #print(key,value)
                    outputDic[key] = value
        else:
            print("File path is not a file.")
            return "File path is not a file."

        return outputDic

