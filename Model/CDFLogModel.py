from collections import OrderedDict
from Model.CSVReader import CSVReader


class CDFLogModel:
    """
    Class outlines the data model. Has functions to load CSV file into a dictionary.
    """

    def __init__(self):
        """
        Constructor.
        """
        self.cdf_dic = OrderedDict()
        self.csv_reader = CSVReader()


    def get_cdf_dic(self):
        """
        returns the diconary in it current state. May return null if called before load_csv_to_dic method.
        :return: dic
        """
        return self.cdf_dic


    def load_csv_to_dic(self, filepath, index):
        """
        Initialize the dictionary with CDF log.
        Index is the column to be used as the key of the dictinoary
        """
        self.cdf_dic = self.csv_reader.readCSVFile(filepath, index)
        return self.cdf_dic

