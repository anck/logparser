import sys, os
from Model.CDFLogModel import CDFLogModel
from View.MainView import MainWindow
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon


class CoreController:
    """
    This the core class. Creates a Model and View Object.
    """

    def __init__(self):
        """
        constructor.
        """
        self.client_model = CDFLogModel()
        self.server_model = CDFLogModel()
        self.window = MainWindow()
        self.window.set_window_icon("../Img/icon.png")
        self.serverFile = str
        self.clientFile = str
        self.init_connectors()

    def init_connectors(self):
        """

        :return:
        """
        self.window.get_server_btn().clicked.connect(self.on_server_open_click)
        self.window.get_client_btn().clicked.connect(self.on_client_open_click)

    def get_client_model(self):
        """
        returns the current model
        """
        return self.client_model

    def get_server_model(self):
        """
        returns the current model
        """
        return self.server_model

    def start(self):
        """
        Main function to start the UI. Initializes the View and Model for the application.
        :return: None.
        """
        cdf_server_dic = self.server_model.load_csv_to_dic("../DemoFiles/exampleCSV.csv", 0)
        cdf_client_dic = self.client_model.load_csv_to_dic("../DemoFiles/exampleCSV.csv", 0)
        self.window.fill_tree(cdf_server_dic, cdf_client_dic)

    def on_client_open_click(self):
        """

        :return:
        """
        filename = self.window.get_file_dialouge().getOpenFileName(self.window.get_file_dialouge(), 'Open File', os.getenv('HOME'))
        print(filename[0])
        cdf_client_dic = self.server_model.load_csv_to_dic(filename[0], 0)
        self.window.fill_client_tree(cdf_client_dic, filename[0])

    def on_server_open_click(self):
        """

        :return:
        """
        filename = self.window.get_file_dialouge().getOpenFileName(self.window.get_file_dialouge(), 'Open File', os.getenv('HOME'))
        print(filename[0])
        cdf_server_dic = self.server_model.load_csv_to_dic(filename[0], 0)
        self.window.fill_server_tree(cdf_server_dic, filename[0])


"""
Code to initialize the application.
"""
app = QApplication(sys.argv)
app.setWindowIcon(QIcon("../Img/icon.png"))
app_ctrl = CoreController()
sys.exit(app.exec_())